from django.apps import AppConfig


class ThebigbathConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TheBigBath'
